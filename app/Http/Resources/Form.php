<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Form extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'type' => new Type($this->type),
            'fields' =>  Field::collection($this->fields),
            'createTime' => (string) $this->created_at,
        ];
    }
}
