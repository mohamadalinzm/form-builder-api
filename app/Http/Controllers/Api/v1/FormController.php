<?php

namespace App\Http\Controllers\Api\v1;

use App\Form;
use App\Http\Controllers\Controller;
use App\Http\Resources\Form as FormResource;
use App\Http\Resources\FormCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FormController extends Controller
{
    public function index()
    {
        $form = Form::all();
        return new FormCollection($form);

    }

    public function single(Form $form)
    {
        return new FormResource($form);
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all() , [
            'title' => 'required|max:255',
            'type_id' => 'required',
            'field' => 'array'
        ]);

        if ($validator->fails()){
            return response()->json(["success" => false,'errors'=>$validator->errors()]);
        }

        $data = $request->fields;

        $mapField =  collect($data)->map(function ($i) {
            return $i;
        });

        $form = Form::create([
            'title' => $request->title,
            'type_id' => $request->type_id,
        ]);

        $form->fields()->sync($mapField);

        return response([
            'data' => [],
            'status' => 'success'
        ],200);
    }

    public function update(Request $request,Form $form)
    {
        $validator = Validator::make($request->all() , [
            'title' => 'required|max:255',
            'type_id' => 'required',
            'field' => 'array'
        ]);

        if ($validator->fails()){
            return response()->json(["success" => false,'errors'=>$validator->errors()]);
        }

        $data = $request->fields;

        $mapField =  collect($data)->map(function ($i) {
            return $i;
        });

        $form->update([
            'title'=>$request->title,
            'type_id' => $request->type_id,
        ]);

        $form->fields()->sync($mapField);

        return response()->json(["success" => true,'req'=>$request->all()]);
    }

    public function destroy(Form $form)
    {
        $form->delete();

        return response()->json(['success' => true,]);
    }
}
