<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\TypeCollection;
use App\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Type as TypeResource;

class TypeController extends Controller
{
    public function index()
    {
        $types = Type::all();
        return new TypeCollection($types);

    }

    public function single(Type $type)
    {
        return new TypeResource($type);
    }

    public function store(Request $request)
    {
        $this->validate($request , [
            'title' => 'required|max:255',
        ]);

        $validator = Validator::make($request->all() , [
            'title' => 'required|max:255',
        ]);

        if ($validator->fails()){
            return response()->json(["success" => false,'errors'=>$validator->errors()]);
        }


        Type::create([
            'title' => $request->title,
        ]);


        return response([
            'data' => [],
            'status' => 'success'
        ],200);
    }

    public function update(Request $request,Type $type)
    {
        $validate = Validator::make($request->all(),[
            'title' => 'required|max:255',
        ]);

        if ($validate->fails()){
            return response()->json(["success" => false,'errors'=>$validate->errors()]);
        }

        $type->update([
            'title'=>$request->title,
        ]);

        return response()->json(["success" => true,'req'=>$request->all()]);
    }

    public function destroy(Type $type)
    {
        $type->delete();

        return response()->json(['success' => true,]);
    }
}
