<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\FieldCollection;
use App\Field;
use App\Http\Resources\Field as FieldResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FieldController extends Controller
{
    public function index()
    {
        $fields = Field::all();
        return new FieldCollection($fields);

    }

    public function single(Field $field)
    {
        return new FieldResource($field);
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all() , [
            'title' => 'required|max:255',
            'type' => 'required|max:255',
        ]);

        if ($validator->fails()){
            return response()->json(["success" => false,'errors'=>$validator->errors()]);
        }

        Field::create([
            'title' => $request->title,
            'type' => $request->type,
        ]);


        return response([
            'data' => [],
            'status' => 'success'
        ],200);
    }

    public function update(Request $request,Field $field)
    {
        $validate = Validator::make($request->all(),[
            'title' => 'required|max:255',
            'type' => 'required|max:255',
        ]);

        if ($validate->fails()){
            return response()->json(["success" => false,'errors'=>$validate->errors()]);
        }

        $field->update([
            'title'=>$request->title,
            'type' => $request->type,
        ]);

        return response()->json(["success" => true,'req'=>$request->all()]);
    }

    public function destroy(Field $field)
    {
        $field->delete();

        return response()->json(['success' => true,]);
    }
}
