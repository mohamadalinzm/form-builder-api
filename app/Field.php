<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $fillable = [
        'title','type'
    ];

    public function forms()
    {
        return $this->belongsToMany(Form::class);
    }
}
