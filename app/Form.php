<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $fillable = [
        'title','type_id'
    ];

    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function fields()
    {
        return $this->belongsToMany(Field::class)->withPivot('name','value');
    }
}
