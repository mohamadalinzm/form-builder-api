<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::prefix('v1')->namespace('Api\v1')->group(function() {

    Route::post('login' , 'UserController@login');
    Route::post('register' , 'UserController@register');

    Route::middleware('auth:api')->group(function() {
        Route::get('/user' , function () {
            return auth()->user();
        });

        Route::get('/types' , 'TypeController@index');
        Route::get('/types/{type}' , 'TypeController@single');
        Route::post('/types' , 'TypeController@store');
        Route::post('/types/{type}' , 'TypeController@update');
        Route::delete('/types/{type}' , 'TypeController@destroy');

        Route::get('/fields' , 'FieldController@index');
        Route::get('/fields/{field}' , 'FieldController@single');
        Route::post('/fields' , 'FieldController@store');
        Route::post('/fields/{field}' , 'FieldController@update');
        Route::delete('/fields/{field}' , 'FieldController@destroy');

        Route::get('/forms' , 'FormController@index');
        Route::get('/forms/{form}' , 'FormController@single');
        Route::post('/forms' , 'FormController@store');
        Route::post('/forms/{form}' , 'FormController@update');
        Route::delete('/forms/{form}' , 'FormController@destroy');

    });
});
